import { smoothScroll } from "./components/smoothScroll";
import { showOnScroll } from "./components/showOnScroll";
import { toggleNavigation } from "./components/toggleNavigation";
import { scrollToTop } from "./components/scrollTotop";
import { stopPhone } from "./components/stopPhone";

stopPhone();
smoothScroll();
showOnScroll();
scrollToTop();
toggleNavigation();
