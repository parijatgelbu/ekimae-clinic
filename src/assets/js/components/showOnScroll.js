function showOnScroll() {
  // const scrollToTop = document.getElementById("js-scrollToTop");
  const fixedBottomNav = document.getElementById("js-fixedBottomNav");

  // if(scrollToTop == null) return;
  if(fixedBottomNav == null) return;

  document.body.style.paddingBottom = fixedBottomNav.offsetHeight + "px";

  window.addEventListener("scroll", function () {
    var windowScroll = window.pageYOffset;
    // let windowHeight = window.innerHeight;

    if (windowScroll > 500) {
      // scrollToTop.style.display = "flex";
      // fixedBottomNav.style.display = "flex";
      fixedBottomNav.classList.add("scroll-show");
    } else {
      // scrollToTop.style.display = "none";
      // fixedBottomNav.style.display = "none";
      fixedBottomNav.classList.remove("scroll-show");
    }

    // if (windowHeight + windowScroll + 80 >= document.body.offsetHeight) {
    //   fixedBottomNav.classList.add("scroll-show");
    // } else {
    //   fixedBottomNav.classList.remove("scroll-show");
    // }
  });
}

export { showOnScroll };
