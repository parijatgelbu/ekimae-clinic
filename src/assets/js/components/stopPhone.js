function stopPhone() {
    const allTelLinks = document.querySelectorAll("a[href*='tel']");
    if (allTelLinks == null) return;
    document.addEventListener("DOMContentLoaded", () => {
        allTelLinks.forEach(telLinks => {
            telLinks.addEventListener("click", event => {
                if(window.innerWidth >= 768) {
                    event.preventDefault();
                }
            });
        });
    });
}

export { stopPhone };